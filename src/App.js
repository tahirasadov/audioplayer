import React from 'react';
import Header from './components/Header';
import Playlist from './components/Playlist';
import Status from './components/Status';
import './App.scss';
import './Fonts.css';
import AppContextProvider from './contexts/AppContext';

function App() {
  return (
    <div className="App">
        <AppContextProvider>
          <Header />
          <Playlist />
          <Status />
        </AppContextProvider>
    </div>
  );
}

export default App;
