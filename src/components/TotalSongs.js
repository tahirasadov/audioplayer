import React, { useContext } from 'react'
import {AppContext}          from './../contexts/AppContext';


const TotalSongs = () => {
    const {TotalSongCount} = useContext(AppContext);
    return (
        <React.Fragment>
            <b>Total:</b> <em>{TotalSongCount} songs</em>
        </React.Fragment>
    )
}

export default TotalSongs