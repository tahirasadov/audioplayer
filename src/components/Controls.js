import React, { useContext } from 'react'

import {AppContext}          from './../contexts/AppContext';

import { FontAwesomeIcon }   from '@fortawesome/react-fontawesome';
import { faBackward }        from '@fortawesome/free-solid-svg-icons';
import { faPlay }            from '@fortawesome/free-solid-svg-icons';
import { faPause }           from '@fortawesome/free-solid-svg-icons';
import { faForward }         from '@fortawesome/free-solid-svg-icons';

const faBackwardIcon         = <FontAwesomeIcon icon={faBackward} />
const faPlayIcon             = <FontAwesomeIcon icon={faPlay} />
const faPauseIcon            = <FontAwesomeIcon icon={faPause} />
const faForwardIcon          = <FontAwesomeIcon icon={faForward} />




const Controls = (props) => {
    const {SongInfo, TogglePlayButton, NextSong, PreviousSong} = useContext(AppContext);
    return (
        <div className="Controls">
            <span className="PrevousSong" onClick={PreviousSong}>{faBackwardIcon}</span>
            {SongInfo.Playing
                 ? <span className="PlayPause" onClick={() => {TogglePlayButton(!SongInfo.Playing)}}>{faPauseIcon}</span>
                 : <span className="PlayPause" onClick={() => {TogglePlayButton(!SongInfo.Playing)}}>{faPlayIcon}</span>
            }
            <span className="NextSong" onClick={NextSong}>{faForwardIcon}</span>
        </div>
    )
}

export default Controls;