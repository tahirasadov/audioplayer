import React, { Component } from 'react'
import CurrentSong          from './CurrentSong';
import Controls             from './../components/Controls'

export default class Header extends Component {
    render() {
        return (
            <div className="Header">
                <Controls />
                <CurrentSong />
            </div>
        )
    }
}
