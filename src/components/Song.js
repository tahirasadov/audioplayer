import React, { useContext } from 'react';
import {AppContext}          from './../contexts/AppContext';


const Song = (props) => {
    const {UpdateCurrentInfo} = useContext(AppContext);
    return (
        <React.Fragment>
            <div className={props.classes} id={props.htmlid} onMouseDown={() => {}} onClick={() => {UpdateCurrentInfo({
                ID:       props.id,
                Art:      props.art_url,
                Title:    props.title,
                Audio:    props.artist,
                Duration: props.duration,
                File:     props.file,
            })}}>
                <span className="Art">
                    <img src={props.art_url} alt="Cover art" />
                </span>
                <span className="Title">{props.title} - {props.artist}</span>
                <span className="Duration">{props.duration}</span>
            </div>
            <div className="SongSeperator"></div>
        </React.Fragment>
    )
}

export default Song;