import React, { Component } from 'react'
import TotalSongs           from './../components/TotalSongs'

export default class Status extends Component {
    render() {
        return (
            <div className="Status">
                <TotalSongs />
            </div>
        )
    }
}
