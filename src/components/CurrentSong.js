import React, { useContext } from 'react'
import Audio                 from './../components/Audio';
import {AppContext}          from './../contexts/AppContext';



const CurrentSong = () => {
    const {SongInfo, Seeker} = useContext(AppContext);
    return (
        <div className="CurrentSong">
        <Audio />
            <span className="CurrentSongArt">
                <img src={SongInfo.Art} alt="" />
            </span>
            <span className="CurrentSongTitle">{SongInfo.Title}</span>
            <span className="CurrentSongDuration">{Seeker} / {SongInfo.Duration}</span>
        </div>
    )
}

export default CurrentSong;