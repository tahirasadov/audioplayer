import React, { useContext } from 'react';
import Songs                 from './../Songs';
import Song                  from './../components/Song';
import {AppContext}          from './../contexts/AppContext';




const Playlist = (props) => {
    const {SongInfo, UpdateCurrentInfo} = useContext(AppContext);

    return (
        <div className="Playlist">
            {Songs.map((SingleSong, i) => {
                const classes = SongInfo.ID == i ? 'Song Selected' : 'Song';
                return (
                    <Song classes={classes} key={i}
                        htmlid   = {'Song-' + i}
                        id       = {i}
                        title    = {SingleSong.title}
                        artist   = {SingleSong.artist}
                        duration = {SingleSong.duration}
                        art_url  = {SingleSong.art}
                        file     = {SingleSong.file}
                    />
                )
            })}
        </div>
    )
}




export default Playlist;