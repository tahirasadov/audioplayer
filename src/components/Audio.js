import React, {useContext} from 'react'
import {AppContext} from './../contexts/AppContext';

const Audio = (props) => {
    const {SongInfo, PlayAudio, PauseAudio, UpdateSeekerValue} = useContext(AppContext);
    return (
        <audio onPlay={() => {PlayAudio()}}  onPause={() => {PauseAudio()}} onTimeUpdate={UpdateSeekerValue} controls>
            <source src={SongInfo.File} type="audio/mpeg"></source>
        </audio>
    )
}
export default Audio;