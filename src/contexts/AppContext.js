import React, {createContext, useState, useEffect} from 'react';
import Songs                 from './../Songs';
import {SecondsToMinute} from './../Helpers';

export const AppContext = React.createContext()

const AppContextProvider = (props) => {

    const DefaultSong = Songs[1];

    const [Seeker, UpdateSeeker] = useState('0:00');

    const [SongInfo, UpdateSongInfo] = useState(
        {
            ID:       DefaultSong.id,
            Art:      DefaultSong.art,
            Title:    DefaultSong.title,
            Audio:    DefaultSong.audio,
            Duration: '0:00',
            File:     DefaultSong.file,
            Playing:  false,
            Ref:      false
        }
    );

    const [TotalSongCount] = useState(Songs.length);

    const TogglePlayButton = (Playing) => {
        let AudioElement = document.getElementsByTagName('audio')[0];
        UpdateSongInfo({...SongInfo, Playing: Playing})
        if(Playing){
            AudioElement.play();
        }else {
            AudioElement.pause();
        }
        UpdateSongInfo({...SongInfo, Duration: SecondsToMinute(AudioElement.duration)});
    }

    const PlayAudio = () => {
        UpdateSongInfo({...SongInfo, Playing: true});
    }

    const PauseAudio = () => {
        UpdateSongInfo({...SongInfo, Playing: false});
    }
    
    const UpdateCurrentInfo = (NewSongInfo) => {
        UpdateSongInfo(NewSongInfo);
        document.getElementsByTagName('audio')[0].load();
        document.getElementsByTagName('audio')[0].play();

    }

    const UpdateSeekerValue = () => {
        let AudioElement = document.getElementsByTagName('audio')[0];
        UpdateSeeker(SecondsToMinute(AudioElement.currentTime));
    }

    const SetNewSong = (ID) => {
        const NewSong = Songs[ID];
        UpdateCurrentInfo({...SongInfo, 
            ID:       NewSong.id,
            Art:      NewSong.art,
            Title:    NewSong.title,
            Audio:    NewSong.audio,
            Duration: NewSong.duration,
            File:     NewSong.file,
        });
    }

    const NextSong = () => {
        if(SongInfo.ID < Songs.length - 1){
            SetNewSong(SongInfo.ID + 1);
        }else {
            SetNewSong(0);
        }
    }
    const PreviousSong = () => {
        if(SongInfo.ID > 0){
            SetNewSong(SongInfo.ID - 1);
        }else {
            SetNewSong(Songs.length - 1);
        }
    }

    return (
        <AppContext.Provider value={{SongInfo, UpdateCurrentInfo, TogglePlayButton, NextSong, PreviousSong, TotalSongCount, PlayAudio, PauseAudio, Seeker, UpdateSeekerValue}}>
            {props.children}
        </AppContext.Provider>
    )

}

export default AppContextProvider;