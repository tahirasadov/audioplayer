const Songs = [
    {
        "id": 0,
        "artist": "Benjamin Tissot",
        "title": "Summer",
        "art": "https://www.bensound.com/bensound-img/summer.jpg",
        "duration": "3:37",
        "file": "/audios/0.mp3"
    },
    {
        "id": 1,
        "artist": "Benjamin Tissot",
        "title": "Ukelele",
        "art": "https://www.bensound.com/bensound-img/ukulele.jpg",
        "duration": "2:26",
        "file": "/audios/1.mp3"
    },
    {
        "id": 2,
        "artist": "Benjamin Tissot",
        "title": "Creative Minds",
        "art": "https://www.bensound.com/bensound-img/creativeminds.jpg",
        "duration": "2:27",
        "file": "/audios/2.mp3"
    },
    {
        "id": 3,
        "artist": "Benjamin Tissot",
        "title": "A New Beginning",
        "art": "https://www.bensound.com/bensound-img/anewbeginning.jpg",
        "duration": "2:34",
        "file": "/audios/3.mp3"
    },
    {
        "id": 4,
        "artist": "Benjamin Tissot",
        "title": "Little Idea",
        "art": "https://www.bensound.com/bensound-img/littleidea.jpg",
        "duration": "2:49",
        "file": "/audios/4.mp3"
    },
    {
        "id": 5,
        "artist": "Benjamin Tissot",
        "title": "Jazzy Frenchy",
        "art": "https://www.bensound.com/bensound-img/jazzyfrenchy.jpg",
        "duration": "1:44",
        "file": "/audios/5.mp3"
    },
    {
        "id": 6,
        "artist": "Benjamin Tissot",
        "title": "Happy Rock",
        "art": "https://www.bensound.com/bensound-img/happyrock.jpg",
        "duration": "1:45",
        "file": "/audios/6.mp3"
    },
    {
        "id": 7,
        "artist": "Benjamin Tissot",
        "title": "Hey!",
        "art": "https://www.bensound.com/bensound-img/hey.jpg",
        "duration": "2:52",
        "file": "/audios/7.mp3"
    },
    {
        "id": 8,
        "artist": "Benjamin Tissot",
        "title": "Cute",
        "art": "https://www.bensound.com/bensound-img/cute.jpg",
        "duration": "3:14",
        "file": "/audios/8.mp3"
    },
    {
        "id": 9,
        "artist": "Benjamin Tissot",
        "title": "Memories",
        "art": "https://www.bensound.com/bensound-img/memories.jpg",
        "duration": "3:50",
        "file": "/audios/9.mp3"
    },
    {
        "id": 10,
        "artist": "Benjamin Tissot",
        "title": "Going Higher",
        "art": "https://www.bensound.com/bensound-img/goinghigher.jpg",
        "duration": "4:04",
        "file": "/audios/10.mp3"
    },
    {
        "id": 11,
        "artist": "Benjamin Tissot",
        "title": "Acoustic Breeze",
        "art": "https://www.bensound.com/bensound-img/acousticbreeze.jpg",
        "duration": "2:37",
        "file": "/audios/12.mp3"
    }
];


export default Songs;